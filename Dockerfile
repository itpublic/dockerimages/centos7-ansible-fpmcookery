FROM centos/ruby-26-centos7

RUN scl enable rh-ruby26 "gem install -N fpm-cookery"

USER root
RUN yum -y install epel-release
RUN curl -sL https://rpm.nodesource.com/setup_12.x | bash -
RUN yum -y update
RUN yum -y install \
    ansible \
    gcc \
    gcc-c++ \
    git \
    make \
    nodejs \
    openssh-clients \
    openssl \
    rpm-build \
    rsync \
    wget \
    which \
 && yum clean all
